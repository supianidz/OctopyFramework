<!DOCTYPE html>
<html lang="{{ $app->locale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="description" content="Octopy Framework">
		<title>Octopy Framework</title>
		<link rel="icon" type="image/x-icon" href="favicon.ico">
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito">
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		@yield('content')
	</body>
</html>